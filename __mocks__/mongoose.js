module.exports = {
  Schema: jest.fn(),
  Error: {
    ValidationError: jest.fn()
  },
  model: jest.fn(),
  connect: jest.fn().mockResolvedValue(),
  set: jest.fn()
};
