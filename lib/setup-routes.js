const Router = require('koa-better-router');
const koaBody = require('koa-body');

const controller = require('./controllers/training');

module.exports = () => {
  let router = Router({ prefix: '/api' }).loadMethods();
  router.get('/training', controller.getTraining);
  router.get('/training/:id', controller.getOne);
  router.post('/training', koaBody(), controller.createTraining);
  router.put('/training/:id', koaBody(), controller.updateTraining);
  router.delete('/training/:id', controller.deleteTraining);
  return router.middleware();
};
