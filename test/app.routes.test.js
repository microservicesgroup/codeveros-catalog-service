const server = require('../app');
const request = require('supertest');

jest.mock('../lib/models/Training');

const Training = require('../lib/models/Training');

// close the server after each test
afterEach(() => {
  server.close();
});

describe('Routes', () => {
  test('GET /api/training should respond as expected', async () => {
    const expected = [ { _id: '1', name: 'Training 1' }, { _id: '2', name: 'Training 2' } ];
    Training.find.mockReturnValueOnce(expected);
    const response = await request(server).get('/api/training');
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expected);
  });

  test('GET /api/training/:id with valid id should respond as expected', async () => {
    const _id = '1';
    const expected = { _id, name: 'Training 1' };
    Training.findById.mockReturnValueOnce(expected);
    const response = await request(server).get(`/api/training/${_id}`);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expected);
  });

  test('GET /api/training/:id with invalid id should return a 404', async () => {
    Training.findById.mockReturnValueOnce(null);
    const response = await request(server).get('/api/training/500');
    expect(response.status).toEqual(404);
  });

  test('POST /api/training should respond as expected', async () => {
    const expected = { _id: '1', name: 'Training 1' };
    Training.create.mockReturnValueOnce(expected);
    const response = await request(server).post('/api/training').send({ name: 'Training 1' });
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expected);
  });

  test('PUT /api/training/:id with valid id should respond as expected', async () => {
    const _id = '1';
    const original = { _id, name: 'Old' };
    const updated = { _id, name: 'New' };
    Training.findById.mockReturnValueOnce(original);
    Training.findByIdAndUpdate.mockReturnValueOnce(updated);
    const response = await request(server).put(`/api/training/${_id}`).send({ name: 'New' });
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(updated);
  });

  test('DELETE /api/training/:id with valid id should respond as expected', async () => {
    const _id = '1';
    const expected = { _id, name: 'Old' };
    Training.findById.mockReturnValueOnce(expected);
    Training.findByIdAndRemove.mockReturnValueOnce(expected);
    const response = await request(server).delete(`/api/training/${_id}`);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expected);
  });
});
